#FROM node:8
FROM arm32v7/node:8.11-onbuild

RUN mkdir /app
RUN mkdir /key
COPY . /app
WORKDIR /app

RUN npm install

CMD ["npm", "start"]
